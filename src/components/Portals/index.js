// MUI Component Imports
import Card from '@mui/material/Card';
import CardContent from '@mui/material/CardContent';
import CardMedia from '@mui/material/CardMedia';
import Box from '@mui/material/Box';
import Grid from '@mui/material/Grid';
import Typography from '@mui/material/Typography';

// Import Projects and Social Info
import based from './based-socials.json'
import cringe from './cringe-socials.json'
import communities from './communities.json'
import projects from './projects.json'
import roster from './roster.json'

// Import Local Images
import imgCinny from '../../assets/cinny-sq.png'
import imgDiscord from '../../assets/discord.png'
import imgMatrix from '../../assets/matrix-sq.png'
import imgGitLab from '../../assets/gitlab.png'
import imgMastodon from '../../assets/mastodon.png'
import imgOdysee from '../../assets/odysee.png'
import imgTwitch from '../../assets/twitch-sq.png'
import imgTwitter from '../../assets/twitter-sq.png'
import imgYouTube from '../../assets/youtube-sq.png'


// Create Img URLs Descriptions
const imgs = {
  "Cinny" : imgCinny,
  "Discord": imgDiscord,
  "Mastodon" : imgMastodon,
  "Matrix" : imgMatrix,
  "Odysee" : imgOdysee,
  "ppesports.pro Website" : imgGitLab,
  "Twitch" : imgTwitch,
  "Twitter" : imgTwitter,
  "YouTube" : imgYouTube
};


const MediaCard = ({project}) => {
  return (
    <Card 
    sx={{ minHeight: 260, maxWidth: 345 }}
    onClick={() => window.open(`${project.url}`, '_blank')}
    style= {{cursor: 'pointer' }}
    >
      <CardMedia
        component="img"
        height="250"
        image={imgs[project.title]}
        alt={project.title}
      />
      <CardContent>
        <Typography gutterBottom variant="h5" component="div">
          {project.title}
        </Typography>
        <Typography variant="body2" color="text.secondary">
          {project.description}
        </Typography>
      </CardContent>
    </Card>
  );
}

const Portals = () => {
  return (
    <Box sx={{ flexGrow: 1 }}>
      {/* Header */}
      <center>
        <Typography variant="h3" gutterBottom color="text.secondary">
          <b>🌙 Welcome to PP_ESport's Link Hub 🌙</b>
        </Typography>
        <Typography variant="h4" gutterBottom color="text.secondary">
          <b>⬇️ Roster ⬇️</b>
        </Typography>
        {roster.map((r) => (
          <div 
            onClick={() => window.open(`${r.url}`, '_blank')}
            style= {{cursor: 'pointer' }}
          >
          <Typography variant="h5" gutterBottom color="text.secondary">
              ⚪  <b>{r.player}</b> - <i>{r.games}</i>
            </Typography>
          </div>
        ))}
      </center>
      <br/>
      {/* Projects */}
      <center>
      <Typography variant="h5" gutterBottom color="text.secondary">
        🌔<i> Communities </i> 🌔
      </Typography>
      </center>
      <Grid container spacing={2}>
        {communities.map((community) => (
          <Grid item xs={12} sm={6} md={4} lg={3}>
            <MediaCard project={community}/>
          </Grid>
        ))}
      </Grid>
      <br/>
      {/* Projects */}
      <center>
      <Typography variant="h5" gutterBottom color="text.secondary">
        🌓<i> BASED Social Media </i> 🌓
      </Typography>
      </center>
      <Grid container spacing={2}>
        {based.map((b) => (
          <Grid item xs={12} sm={6} md={4} lg={3}>
            <MediaCard project={b}/>
          </Grid>
        ))}
      </Grid>
      <br/>
      {/* Projects */}
      <center>
      <Typography variant="h5" gutterBottom color="text.secondary">
        🌒<i> CRINGE Social Media </i> 🌒
      </Typography>
      </center>
      <Grid container spacing={2}>
        {cringe.map((c) => (
          <Grid item xs={12} sm={6} md={4} lg={3}>
            <MediaCard project={c}/>
          </Grid>
        ))}
      </Grid>
      <br/>
      {/* Socials */}
      <center>
      <Typography variant="h5" gutterBottom color="text.secondary">
        🌃 <i> Open Source Projects </i> 🌃
        </Typography>
      </center>
      <br/>
      <Grid container spacing={2}>
        {projects.map((project) => (
          <Grid item xs={12} sm={6} md={4} lg={3}>
            <MediaCard project={project}/>
          </Grid>
        ))}
      </Grid>
      <br/>
    </Box>
  );
}

export default Portals